import { Component, OnInit } from '@angular/core';

export class NgxQrCode {
  text: string;

}

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.css']
})
export class GeneratorComponent implements OnInit {

  constructor() {
    console.log('AppComponent running');
    this.qrdata = 'https://www.xing.com/profile/name';
  }

  public qrdata: string = null;


  ngOnInit() {
  }
  //
  // changeValue(newValue: string): void {
  //   this.qrdata = newValue;
  // }

  saveQR() {
    console.log('gespeichert');
  }

  clear() {
    console.log('weg damit');
    this.qrdata = '';

  }

}
